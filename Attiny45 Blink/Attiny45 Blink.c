/*
 * Attiny45_Blink.c
 *
 * Created: 6/1/2015 18:53:25
 *  Author: Brandy
 */ 


#include <avr/io.h>
#define F_CPU 1000000UL
#include <util/delay.h>

int main(void)
{
	DDRB = (0 << DDB5)|(0 << DDB4)|(1 << DDB3)|(1 << DDB2)|(1 << DDB1)|(1 << DDB0);
    while(1)
    {
		PINB |= (1 << PINB3)|(1 << PINB2)|(1 << PINB1)|(1 << PINB0);
		//Toogles The PIN
		_delay_ms(500);
		//Waits for 500miliSeconds
		//TODO:: Please write your application code
	}

}